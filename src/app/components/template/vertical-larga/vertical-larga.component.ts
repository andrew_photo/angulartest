import { Component, OnInit, Input } from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-vertical-larga',
  templateUrl: './vertical-larga.component.html',
  styleUrls: ['./vertical-larga.component.css']
})
export class VerticalLargaComponent implements OnInit {

  @Input() forma: FormGroup;

  constructor() { }

  ngOnInit(): void {
  }

}
