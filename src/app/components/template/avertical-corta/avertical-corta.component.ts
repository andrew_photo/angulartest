import { Component, OnInit, Input } from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-avertical-corta',
  templateUrl: './avertical-corta.component.html',
  styleUrls: ['./avertical-corta.component.css']
})
export class AverticalCortaComponent implements OnInit {

  @Input() forma: FormGroup;

  constructor() { }

  ngOnInit(): void {
  }

}
