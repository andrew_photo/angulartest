import { Component, OnInit, Input } from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-vertical-corta',
  templateUrl: './vertical-corta.component.html',
  styleUrls: ['./vertical-corta.component.css']
})
export class VerticalCortaComponent implements OnInit {

  @Input() forma: FormGroup;

  constructor() { }

  ngOnInit(): void {
  }

}
