import { Component, OnInit, Input } from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-avertical-larga',
  templateUrl: './avertical-larga.component.html',
  styleUrls: ['./avertical-larga.component.css']
})
export class AverticalLargaComponent implements OnInit {

  @Input() forma: FormGroup;

  constructor() { }

  ngOnInit(): void {
  }

}
