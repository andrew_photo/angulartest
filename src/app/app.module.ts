import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VerticalCortaComponent } from './components/template/vertical-corta/vertical-corta.component';
import { VerticalLargaComponent } from './components/template/vertical-larga/vertical-larga.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatExpansionModule} from '@angular/material/expansion';
import { AverticalLargaComponent } from './components/template/avertical-larga/avertical-larga.component';
import { AverticalCortaComponent } from './components/template/avertical-corta/avertical-corta.component';

@NgModule({
  declarations: [
    AppComponent,
    VerticalCortaComponent,
    VerticalLargaComponent,
    AverticalLargaComponent,
    AverticalCortaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatExpansionModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
