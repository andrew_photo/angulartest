import { Component, OnInit} from '@angular/core';
import {jsPDF} from 'jspdf';
import html2canvas from 'html2canvas';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//ng build --prod --base-href="./"
//git commit -m "DES-5 pipeline"
//git push -u origin master

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'html-to-pdf';

  imagenCreada;
  imgcreada = false;
 
  forma: FormGroup;
  constructor(private fb: FormBuilder) { 
    this.crearFormulario();
  }

  ngOnInit(): void {
  }

  get tipoEtiquetaNoValida(){
    return this.forma.get('tipoEtiqueta').invalid;
  }

  get genEtiquetaNoValida(){
    return this.forma.get('genEtiqueta').invalid;
  }
  get tamanoPorcioNoValida(){
    return this.forma.get('tamanoPorcion').invalid && this.forma.get('tamanoPorcion').touched;
  }
  get cantidadPorcionesNoValida(){
    return this.forma.get('cantidadPorciones').invalid && this.forma.get('cantidadPorciones').touched;
  }
  get conversionPorcionesNoValida(){
    return this.forma.get('conversionPorciones').invalid && this.forma.get('conversionPorciones').touched;
  }
  get caloriasNoValida(){
    return this.forma.get('calorias').invalid && this.forma.get('calorias').touched;
  }
  get proteinasNoValida(){
    return this.forma.get('proteinas').invalid && this.forma.get('proteinas').touched;
  }
  get grasaTotalNoValida(){
    return this.forma.get('grasaTotal').invalid && this.forma.get('grasaTotal').touched;
  }
  get carboDisponiblesNoValida(){
    return this.forma.get('carboDisponibles').invalid && this.forma.get('carboDisponibles').touched;
  }
  get azucaresTotalesNoValida(){
    return this.forma.get('azucaresTotales').invalid && this.forma.get('azucaresTotales').touched;
  }
  get fibraDietaTotalNoValida(){
    return this.forma.get('fibraDietaTotal').invalid && this.forma.get('fibraDietaTotal').touched;
  }
  get sodioNoValida(){
    return this.forma.get('sodio').invalid && this.forma.get('sodio').touched;
  }
  get grasasSaturadasNoValida(){
    return this.forma.get('grasasSaturadas').invalid && this.forma.get('grasasSaturadas').touched;
  }
  get grasasMonoInsaturadasNoValida(){
    return this.forma.get('grasasMonoInsaturadas').invalid && this.forma.get('grasasMonoInsaturadas').touched;
  }
  get grasasTransNoValida(){
    return this.forma.get('grasasTrans').invalid && this.forma.get('grasasTrans').touched;
  }
  get grasasPoliInsaturadasNoValida(){
    return this.forma.get('grasasPoliInsaturadas').invalid && this.forma.get('grasasPoliInsaturadas').touched;
  }
  get colesterolNoValida(){
    return this.forma.get('colesterol').invalid && this.forma.get('colesterol').touched;
  }
  get contenidoNetoNoValida(){
    return this.forma.get('contenidoNeto').invalid && this.forma.get('contenidoNeto').touched;
  }
  get nombreRazonSocialNoValida(){
    return this.forma.get('nombreRazonSocial').invalid && this.forma.get('nombreRazonSocial').touched;
  }
  get plazoDuracionNoValida(){
    return this.forma.get('plazoDuracion').invalid && this.forma.get('plazoDuracion').touched;
  }
  get importadoPorNoValida(){
    return this.forma.get('importadoPor').invalid && this.forma.get('importadoPor').touched;
  }
  get paisOrigenNoValida(){
    return this.forma.get('paisOrigen').invalid && this.forma.get('paisOrigen').touched;
  }
  get modoUsoConsumoNoValida(){
    return this.forma.get('modoUsoConsumo').invalid && this.forma.get('modoUsoConsumo').touched;
  }
  get condicionesAlmacenamientoNoValida(){
    return this.forma.get('condicionesAlmacenamiento').invalid && this.forma.get('condicionesAlmacenamiento').touched;
  }
  get descripcionIngredientesNoValida(){
    return this.forma.get('descripcionIngredientes').invalid && this.forma.get('descripcionIngredientes').touched;
  }
  get resolucionSanitariaNoValida(){
    return this.forma.get('resolucionSanitaria').invalid && this.forma.get('resolucionSanitaria').touched;
  }
  get nombreProductoNoValida(){
    return this.forma.get('nombreProducto').invalid && this.forma.get('nombreProducto').touched;
  }
  crearFormulario(){

    this.forma = this.fb.group({
      tipoEtiqueta: ['',[Validators.required,Validators.nullValidator]],//siempre - Paso 1
      sellos:[''],//actualizada - Paso 2
      genEtiqueta: ['',[Validators.required,Validators.nullValidator]],//siempre - Paso 2
      tamanoPorcion: ['',[Validators.required,Validators.min(1)]],//siempre - Paso 3
      cantidadPorciones: ['',[Validators.required,Validators.min(1)]],//siempre - Paso 3
      conversionPorciones: ['',[Validators.required,Validators.nullValidator]],//siempre - Paso 3
      calorias: ['',[Validators.required,Validators.min(1)]],//siempre - Paso 3
      proteinas: ['',[Validators.required,Validators.min(1)]],//siempre - Paso 3
      grasaTotal: ['',[Validators.required,Validators.min(1)]],//siempre - Paso 3
      carboDisponibles: ['',[Validators.required,Validators.min(1)]],//siempre - Paso 3
      azucaresTotales: ['',[Validators.required,Validators.min(1)]],//siempre - Paso 3
      fibraDietaTotal: ['',[Validators.required,Validators.min(1)]],//siempre - Paso 3
      sodio: ['',[Validators.required,Validators.min(1)]],//siempre - Paso 3
      grasasSaturadas: ['',[Validators.required,Validators.min(1)]],//Larga - Paso 3
      grasasMonoInsaturadas: ['',[Validators.required,Validators.min(1)]],//Larga - Paso 3
      grasasTrans: ['',[Validators.required,Validators.min(1)]],//Larga - Paso 3
      grasasPoliInsaturadas: ['',[Validators.required,Validators.min(1)]],//Larga - Paso 3
      colesterol: ['',[Validators.required,Validators.min(1)]],//Larga - Paso 3
      contenidoNeto: ['',[Validators.required,Validators.min(1)]],//siempre - Paso 4
      nombreRazonSocial: ['',[Validators.required,Validators.minLength(3)]],//siempre - Paso 4
      plazoDuracion: ['',[Validators.required,Validators.minLength(5)]],//siempre - Paso 4
      importadoPor: ['',[Validators.required,Validators.minLength(5)]],//siempre - Paso 4
      paisOrigen: ['',[Validators.required,Validators.minLength(3)]],//siempre - Paso 4
      modoUsoConsumo: ['',[Validators.required,Validators.minLength(5)]],//siempre - Paso 4
      condicionesAlmacenamiento: ['',[Validators.required,Validators.minLength(5)]],//siempre - Paso 4
      descripcionIngredientes: ['',[Validators.required,Validators.minLength(3)]],//siempre - Paso 4
      resolucionSanitaria: ['',[Validators.required,Validators.minLength(1)]],//siempre - Paso 4
      nombreProducto:['',[Validators.required,Validators.minLength(4)]],//siempre - Paso 4
    });

  }

  guardar(){
    console.log(this.forma);
  }

  crearImagen() {
    window.scrollTo(0, 0);//Se deja en 0,0 para evitar imagen movida
    console.log("ordeno imagen");
    html2canvas(document.getElementById('contentToConvert'),{
      onclone: function (clonedDoc) {
    
         // I made the div hidden and here I am changing it to visible
        clonedDoc.getElementById('contentToConvert').style.visibility = 'visible';
      }
    }
      ).then(canvas => {
 
      this.imagenCreada = canvas.toDataURL(); 

      this.imgcreada = true;     
    });
  }

  generatePDF() {
    window.scrollTo(0, 0);//Se deja en 0,0 para evitar imagen movida
    html2canvas(document.getElementById('contentToConvert'),{
      onclone: function (clonedDoc) {
    
         // I made the div hidden and here I am changing it to visible
        clonedDoc.getElementById('contentToConvert').style.visibility = 'visible';
      }
    }
      ).then(canvas => {
      var imgWidth = 18;
      var imgHeight = 10;
      //var imgHeight = canvas.height * imgWidth / canvas.width;
      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'cm');
      var position = 1;
      pdf.addImage(contentDataURL, 'PNG', 1, position, imgWidth, imgHeight)
      pdf.save('etiqueta.pdf');
    });
  }
  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep1() {
    if(!this.tipoEtiquetaNoValida){
      this.step++;
    }
  }

  nextStep2() {
    if(!this.genEtiquetaNoValida){
      this.step++;
    }
  }

  nextStep3() {
    if(!this.tipoEtiquetaNoValida){
      this.step++;
    }
  }

  nextStep4() {
    if(!this.tipoEtiquetaNoValida){
      this.step++;
    }
  }

  fin() {
   // if(this.forma.valid){
    if(!this.tipoEtiquetaNoValida){
      this.step++;
      console.log("habilito panel");
    }
  }
  prevStep() {
    this.step--;
  }
}